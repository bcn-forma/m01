---
title: "Seguridad Informática"
date: 2018-12-29T11:02:05+06:00
type: "post"
weight : 2
---

- Introducción a los datos.
- Uso y gestión de contraseñas con KeePass.
- Tipos de malware.
- Prevención de la ingeniería social.