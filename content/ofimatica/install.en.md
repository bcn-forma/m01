---
title: "Libre Office Writer"
date: 2018-12-29T11:02:05+06:00
type: "post"
weight : 2
---

- Introducción a Writer.
- Escribir y editar texto.
- Dar formato al texto.
- Configurar el entorno de trabajo de LibreOffice.
- Configuración de página.
- Formato avanzado de texto.
- Uso de tablas.
- Insertar imágenes.
- Imprimir documentos.