---
title: "Libre Office Writer"
date: 2018-12-29T11:02:05+06:00
type: "post"
weight : 2
---

- Introduction to Writer.
- Write and edit text.
- Format the text.
- Configure the LibreOffice work environment.
- Page configuration.
- Advanced text format.
- Use of tables.
- Insert images.
- Print documents.