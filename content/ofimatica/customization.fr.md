---
title: "Libre Office Draw"
date: 2018-12-29T11:02:05+06:00
type: "post"
---

- Default figures.
- Styles and groups.
- Effects on images.
- Zoom and printing.