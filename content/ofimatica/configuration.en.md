---
title: "Libre Office Calc"
date: 2018-12-29T11:02:05+06:00
type: "post"
weight: 3
---

- Introducción a Calc.
- Introducción, edición y formato de datos.
- Creación de tablas y gráficos.
- Uso de estilos y plantillas.
- Uso de gráficos.
- Impresión, exportación y envío por correo electrónico.
- Uso de fórmulas y funciones.
- Uso de tablas dinámicas.
- Análisis de datos.
- Enlace de datos de cálculo.
- Compartir y revisar documentos.
- Calc Macros.
- Calc como una base de datos simple.
- Configuración y personalización de Calc.