---
title: "Libre Office Calc"
date: 2018-12-29T11:02:05+06:00
type: "post"
weight: 3
---

- Introduction to Calc.
- Introduction, edition and data format.
- Creation of tables and graphs.
- Use of styles and templates.
- Use of graphics.
- Printing, export and sending by email.
- Use of formulas and functions.
- Use of dynamic tables.
- Analysis of data.
- Calculation data link.
- Share and review documents.
- Calc Macros.
- Calc as a simple database.
- Configuration and personalization of Calc.