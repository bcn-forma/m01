---
title: "Python"
date: 2018-12-29T11:02:05+06:00
type: "post"
---

- Introducción a Python.

- Sintaxis básica de Python.

- String y salidas de consola.

- Condicionales y control de flujo.