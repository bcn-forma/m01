---
title: "Python"
date: 2018-12-29T11:02:05+06:00
type: "post"
---

- Introduction to Python.

- Python basic syntax.

- String and console outputs.

- Conditional and flow control.