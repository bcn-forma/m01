---
title: "CSS"
date: 2018-12-29T11:02:05+06:00
type: "post"
weight: 3
---

- Introducción.
- Sintaxis.
- Selectores simples.
- Modelo de caja.
- Posicionamiento.
- Medias.